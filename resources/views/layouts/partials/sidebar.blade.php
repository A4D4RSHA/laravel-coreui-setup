<ul class="c-sidebar-nav">
<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link" href="/">
    <svg class="c-sidebar-nav-icon">
        <use
        xlink:href="vendors/@coreui/icons/svg/free.svg#cil-home"
        ></use>
    </svg>
    <span>Home</span>
    </a>
</li>

<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link" href="/">
    <svg class="c-sidebar-nav-icon">
        <use
        xlink:href="vendors/@coreui/icons/svg/free.svg#cil-layers"
        ></use>
    </svg>
    <span>Categories</span>
    </a>
</li>

<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link" href="/">
    <svg class="c-sidebar-nav-icon">
        <use
        xlink:href="vendors/@coreui/icons/svg/free.svg#cil-truck"
        ></use>
    </svg>
    <span>Products</span>
    </a>
</li>

<li class="c-sidebar-nav-item">
    <a class="c-sidebar-nav-link" href="/">
    <svg class="c-sidebar-nav-icon">
        <use
        xlink:href="vendors/@coreui/icons/svg/free.svg#cil-tags"
        ></use>
    </svg>
    <span>Attributes</span>
    </a>
</li>


{{-- <li class="c-sidebar-nav-title">Item</li> --}}

<!-- Dropdown Sample -->
{{-- <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
    <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
        <svg class="c-sidebar-nav-icon">
            <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-puzzle"></use>
        </svg>
        <span>Category</span>
    </a>
    <ul class="c-sidebar-nav-dropdown-items">
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="base/breadcrumb.html"
        ><span class="c-sidebar-nav-icon"></span> Breadcrumb</a
        >
    </li>
    </ul>
</li> --}}
</ul>