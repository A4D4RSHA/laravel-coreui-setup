<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', $title ?? config('app.name'))</title>
    <link rel="stylesheet" href="{{ asset('css/coreui.min.css') }}">
    @yield('css')
</head>
<body class="c-app">

    <div
    class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show"
    id="sidebar"
  >
    @include('layouts.partials.sidebar')
    <button
      class="c-sidebar-minimizer c-class-toggler"
      type="button"
      data-target="_parent"
      data-class="c-sidebar-minimized"
    ></button>
  </div>
  <div class="c-wrapper c-fixed-components">
    <header
      class="c-header c-header-light c-header-fixed c-header-with-subheader"
    >
      <button
        class="c-header-toggler c-class-toggler d-lg-none mfe-auto"
        type="button"
        data-target="#sidebar"
        data-class="c-sidebar-show"
      >
        <svg class="c-icon c-icon-lg">
          <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
        </svg>
      </button>
      <a class="c-header-brand d-lg-none" href="#">
        <svg width="118" height="46" alt="CoreUI Logo">
          <use xlink:href="assets/brand/coreui.svg#full"></use>
        </svg>
      </a>
      <button
        class="c-header-toggler c-class-toggler mfs-3 d-md-down-none"
        type="button"
        data-target="#sidebar"
        data-class="c-sidebar-lg-show"
        responsive="true"
      >
        <svg class="c-icon c-icon-lg">
          <use xlink:href="vendors/@coreui/icons/svg/free.svg#cil-menu"></use>
        </svg>
      </button>
      <ul class="c-header-nav d-md-down-none">
        <li class="c-header-nav-item px-3">
          <a class="c-header-nav-link" href="#">Back to Site</a>
        </li>
      </ul>
      <ul class="c-header-nav ml-auto mr-4">
        <li class="c-header-nav-item dropdown">
          <a
            class="c-header-nav-link"
            data-toggle="dropdown"
            href="#"
            role="button"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <div class="c-avatar">
              <img
                class="c-avatar-img"
                src="https://randomuser.me/api/portraits/women/44.jpg"
                alt="user@email.com"
              />
            </div>
          </a>
          <div class="dropdown-menu dropdown-menu-right pt-0">
            <a class="dropdown-item" href="#">
              <svg class="c-icon mr-2">
                <use
                  xlink:href="vendors/@coreui/icons/svg/free.svg#cil-user"
                ></use>
              </svg>
              Profile
            </a>

            <a class="dropdown-item" href="#">
                <svg class="c-icon mr-2">
                  <use
                    xlink:href="vendors/@coreui/icons/svg/free.svg#cil-power-standby"
                  ></use>
                </svg>
                Logout
              </a>
          </div>
        </li>
      </ul>
    </header>
    <div class="c-body">
      <main class="c-main">
        <div class="container-fluid">
          <div class="fade-in">
            <!-- Content -->
            @yield('content')
          </div>
        </div>
      </main>
      <footer class="c-footer">
        <div>Nice Project</div>
        <div class="ml-auto">
          Copyright &copy; {{ date("Y") }} - {{ config('app.name') }}
        </div>
      </footer>
    </div>
  </div>


    <!-- Js -->
    <script src="{{ asset('js/coreui.bundle.min.js') }}"></script>
    @yield('js')
</body>
</html>